﻿using System;

namespace Task4
{
    class Task4
    {
        static void Main(string[] args)
        {
            SquareMatrix matrixOne = new SquareMatrix(3);
            matrixOne.Notify += DisplayMessage;
            matrixOne.Initialize();
            Console.WriteLine(matrixOne.ToString());
            
            SquareMatrix matrixTwo = new SquareMatrix(3);
            matrixTwo.Initialize();
            Console.WriteLine(matrixTwo.ToString());
            matrixOne = matrixOne * matrixTwo;
            Console.WriteLine(matrixOne.ToString());
            matrixOne[1, 2] = 3;
            //bool check = SquareMatrix.Equals(matrixOne, matrixTwo);
            //bool check = matrixOne.Equals(matrixTwo);
            //Console.WriteLine(check)
            //SquareMatrix matrixResult = matrixOne + matrixTwo;
            //Console.WriteLine(matrixResult.ToString());
            //SquareMatrix matrix = new SquareMatrix(3);
            //matrix.Initialize();
            //Console.WriteLine(matrix.ToString());
            //int rez = matrix["2", "1"];
            //Console.WriteLine(rez);
            //DiagonatMatrix diagonatMatrix = new DiagonatMatrix(3);
            //diagonatMatrix.Initialize();
            //diagonatMatrix["1", "2"] = 5;
            //Console.WriteLine(diagonatMatrix.ToString());
            //Console.WriteLine();
            // UpperTriangleMatrix upperTriangleMatrix = new UpperTriangleMatrix(3);
            //upperTriangleMatrix.Initialize();
            //upperTriangleMatrix["4", "1"] = 5;
            //Console.WriteLine(upperTriangleMatrix.ToString());


        }
        private static void DisplayMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
