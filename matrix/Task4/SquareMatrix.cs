﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace Task4
{
   public class SquareMatrix
   {
       private int[] _data;
       private int _size;
       public delegate void Matrix(string message);
       public event Matrix Notify;
       public SquareMatrix(int size)
       {
            _size = size;
            _data = new int[_size * _size];
       }
        protected SquareMatrix()
        {

        }
       public virtual void Initialize()
       {
            
           Random random = new Random();
           for(int i = 0; i < _size * _size; i++)
           {
               _data[i] = random.Next(1,4);
                
           }
            
       }
       public virtual int this[int i, int j]
       {
            get
            {
                while (i >= _size || j >= _size)
                {
                    throw new Exception("Out of range");
                }
                return _data[i*_size +j];
            }
            set
            {
                int number = _data[i * _size + j];
                _data[i* _size +j] = value;
                Notify?.Invoke($"element[{i},{j}] changed from {number} to {value}");
            }
        }
        public int this[string row, string coloumn]
        {
            get
            {
                int i = Convert.ToInt32(row);
                int j = Convert.ToInt32(coloumn);
                return this[i,j];
            }
            set
            {
                int i = Convert.ToInt32(row);
                int j = Convert.ToInt32(coloumn);
                this[i, j] = value;
            }
        }
        public override string ToString()
        {
            string result = string.Empty;
            for(int i = 0; i < _size; i++)
            {
                for(int j = 0; j < _size; j++)
                {
                    result += this[i, j] + " ";
                }
                result += "\n";
            }
            return result;
        }
        //public static SquareMatrix MatrixMultiplication(SquareMatrix matrixOne, SquareMatrix matrixTwo)
        //{
        //    SquareMatrix matrixResult = new SquareMatrix(matrixOne._size);
        //    if (matrixOne._size != matrixTwo._size)
        //    {
        //        throw new Exception("Not the same matrix");
        //    }
        //    for(int i = 0; i < matrixOne._size; i++)
        //    {
        //        for (int j = 0; j <matrixTwo._size; j++)
        //        {
                    
        //            for(int k = 0; k <matrixOne._size; k++)
        //            {
        //                matrixResult[i, j] += matrixOne[i, k] * matrixTwo[k, j];
        //            }
        //        }
        //    }
        //    return matrixResult;
        //}
        public static SquareMatrix operator *(SquareMatrix matrixOne, SquareMatrix matrixTwo)
        {
            SquareMatrix matrixResult = new SquareMatrix(matrixOne._size);
            if (matrixOne._size != matrixTwo._size)
            {
                throw new Exception("Not the same matrix");
            }
            for (int i = 0; i < matrixOne._size; i++)
            {
                for (int j = 0; j < matrixTwo._size; j++)
                {

                    for (int k = 0; k < matrixOne._size; k++)
                    {
                        matrixResult[i, j] += matrixOne[i, k] * matrixTwo[k, j];
                    }
                }
            }
            for (int i = 0; i < matrixOne._size; i++)
            {
                for (int j = 0; j < matrixOne._size; j++)
                {
                    matrixOne[i, j] = matrixResult[i, j];
                }
            }
            return matrixResult;
        }
        public static SquareMatrix operator +(SquareMatrix matrixOne, SquareMatrix matrixTwo)
        {
            SquareMatrix matrixResult = new SquareMatrix(matrixOne._size);
            if (matrixOne._size != matrixTwo._size)
            {
                throw new Exception("Not the same matrix");
            }
            for (int i = 0; i < matrixOne._size; i++)
            {
                for (int j = 0; j < matrixTwo._size; j++)
                {
                        matrixResult[i, j] += matrixOne[i, j] + matrixTwo[i, j];
                }
            }
            return matrixResult;
        }
       
        public static  bool Equals(SquareMatrix matrixOne, SquareMatrix matrixTwo)
        {
            for (int i = 0; i < matrixOne._size; i++)
            {
                for (int j = 0; j < matrixTwo._size; j++)
                {
                    if (matrixOne[i, j] != matrixTwo[i, j])
                        return false;
                }
            }
            return true;
        }
        public  bool Equals(SquareMatrix matrix)
        {
            for (int i = 0; i <_size; i++)
            {
                if(this._data[i] != matrix._data[i])
                {
                    return false;
                }    
            }
            return true;
        }
    }
    
}
